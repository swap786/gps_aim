import java.io.*;
import java.net.*;
import jssc.*;


class UDPClient
{
   public static void main(String args[]) throws Exception
   {	   
      DatagramSocket clientSocket = new DatagramSocket();
      InetAddress IPAddress = InetAddress.getByName("59.90.140.204");
      byte[] sendData = new byte[1024];      
      String sentence = "TCP";
      sendData = sentence.getBytes();
      DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 2020);	
      clientSocket.send(sendPacket);
	  SerialPort serialPort=null;
	  try{
	  serialPort = new SerialPort(args[0]);
	  
	  serialPort.openPort();
	  serialPort.setParams(SerialPort.BAUDRATE_9600,
                         SerialPort.DATABITS_8,
                         SerialPort.STOPBITS_1,
                         SerialPort.PARITY_NONE);

    serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | 
                                  SerialPort.FLOWCONTROL_RTSCTS_OUT);
   }							  
	catch (SerialPortException ex) {
    System.out.println("There are an error on writing string to port т: " + ex);
	}
	  
	  while(true){
	  byte[] receiveData = new byte[1024];
      DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
      clientSocket.receive(receivePacket);
      String modifiedSentence = new String(receivePacket.getData());
      System.out.println("FROM SERVER:" + modifiedSentence);
	  
	    
try {
 
    serialPort.writeString(modifiedSentence);
}
catch (SerialPortException ex) {
    System.out.println("There are an error on writing string to port т: " + ex);
}
	  
	  
      //clientSocket.close();
	   }
   }
}
